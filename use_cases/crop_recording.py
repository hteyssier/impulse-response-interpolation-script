import os
import numpy as np
import soundfile as sf

# here, put the relative path of where the IRs to edit are located. 
source_directory = "centech/"
abs_path_to_dir = os.path.abspath(source_directory)

for filename in os.listdir(abs_path_to_dir):
    
    # Select the files you want to edit. 
    if filename[-1] == 'v' and filename[-2] == 'a':
        
        # reading the file and storing it in an np.ndarray 
        output, samplerate = sf.read(abs_path_to_dir+"/"+filename)
        number_of_frames, number_of_channels = output.shape
        output = output.T

        # start and end time of the IR to edit (in seconds). 
        start = 0.45
        #end = 13
        first_frame = int(samplerate * start)
        last_frame = number_of_frames - 1

        total_length = last_frame - first_frame
        data = np.ndarray((number_of_channels, total_length))

        # copying the frames that we want from the file into the data array
        for channel in range(number_of_channels):
            data[channel] = output[channel][first_frame:last_frame]
            
        new_dir = "centech_edited/"
        abs_path_to_new_dir = os.path.abspath(new_dir)
        new_name = abs_path_to_new_dir+"/"+filename

        # edited IRs will be written in the current working directory. 
        sf.write(new_name, data.T, samplerate)