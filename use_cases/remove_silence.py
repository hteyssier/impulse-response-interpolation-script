import os
import numpy as np
import soundfile as sf
import matplotlib.pyplot as plt

#filename = "/home/hteyssier/Documents/IR_MaisonSymphonique/Atelier7Hz_monoIRs/IR session 2/IR_Iph16-session2.45_S01_mono.wav"

# here, put the relative path of where the IRs to edit are located. 
source_directory = "ambisonic_irs_MS/iteration3_fixed/"
abs_path_to_dir = os.path.abspath(source_directory)

for filename in os.listdir(abs_path_to_dir):

    # reading the file and storing it in an np.ndarray 
    output, samplerate = sf.read(abs_path_to_dir+"/"+filename)
    number_of_frames, number_of_channels = output.shape

    output = output.T 

    start = 0

    chunk = 50
    silence_threshold = 0.05
    delay_before_ir = 500
    delay_after_ir = 100000

    # finding the begining of the IR in recording
    while start < len(output[0]):
        ratio = 0
        for value in output[0][start : start+chunk]:
            if abs(value) > silence_threshold:
                ratio += 1/chunk

        # if more than 3/5 of values in the current chunk are above the silent threshold, 
        # then we consider that it is the beginning of the IR.
        if ratio > 3/5:
            break

        start += chunk

    first_frame = start - delay_before_ir

    silence_threshold = 0.0005

    # finding end of the IR in recording
    while start < len(output[0]) :
        ratio = 0
        for value in output[0][start : start+chunk]:
            if abs(value) < silence_threshold:
                ratio += 1/chunk

        # if more than 7/8 of values in the current chunk are below the silent threshold, 
        # then we consider that it is the end of the IR.
        if ratio > 7/8:
            break

        start += chunk

    last_frame = start + delay_after_ir

    total_length : int = last_frame - first_frame

    # if the total_lenght is below 100 samples, then the script did not find the begening of the IR.
    # One fix could be to change the silence_threshold.
    if total_length < 100 + delay_before_ir or first_frame + delay_before_ir >= number_of_frames:
        print("Error on file: "+filename)
        continue

    data = np.ndarray((number_of_channels, total_length))

    for channel in range(number_of_channels):
        # copying the frames that we want from the file into the data array.
        data[channel] = output[channel][first_frame:last_frame]

    # figs, arr = plt.subplots(2)
    # arr[0].plot(output)
    # arr[1].plot(data)
    # plt.show()

    new_dir = "ambisonic_irs_MS/iteration3_croped/"
    abs_path_new_dir = os.path.abspath(new_dir)
    new_name = abs_path_new_dir+"/"+filename.replace(".wav", "_edited.wav")

    sf.write(new_name, data.T, samplerate)
    
    print("Done with "+filename)
