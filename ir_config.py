#!/usr/bin/python3
import math
from matplotlib.pyplot import fill
import numpy as np
import soundfile as sf
import os

from dataclasses import dataclass
from typing import Tuple

class Source:
    def __init__(self, pFile: str, pCoords: Tuple[float, float, float]):
        self._a_file : str = pFile
        self._x : float = pCoords[0]
        self._y : float = pCoords[1]
        self._z : float = pCoords[2]


class Mic:

    def __init__(self, pFile: str, pCoords: Tuple[float, float, float]):
        self._a_file : str = pFile
        self._x : float = pCoords[0]
        self._y : float = pCoords[1]
        self._z : float = pCoords[2]
        self._distance_to_source : float = 0.0

    def get_distance_with(self, src: Source) -> float:
        """A method that returns the distance in space between the current Point instance and the IR instance passed as argument.

        Args:
            anIR (IR): the impulse reponse for which we want to know the distance.

        Returns:
            float: the euclidian distance between the two points.
        """

        self._distance_to_source = math.sqrt(
            (src._x - self._x) ** 2
            + (src._y - self._y) ** 2
            + (src._z - self._z) ** 2
        )


class IR_Config:
    """Represents an Impulse Response using numpy arrays."""

    @dataclass
    class IR:
        """A class to represent the basic parameters of an Impulse Response object."""

        a_file: str
        x: float
        y: float
        z: float

    def __init__(self, pFile: str, pCoords: Tuple[float, float, float]):
        """Initializes an IR instance, subtype of Point instance. An IR instance can correspond to a multichannel recording (e.g., ambisonic).
           An IR instance has 12 attributes:
           - a filename,
           - three floats for the coordinates in space,
           - an array of float for the amplitude at a particular position,
           - a float for the distance to the wanted point,
           - a float for the _weight,
           - an integer variable for the number of channels,
           - a 2d array of float for the early reflections,
           - a 2d array of float for the tail,
           - a 2d merged array,
           - a 2d array of int for the warp tables.

        Args:
            pFile (str): path to an IR file.
            pCoords (Tuple[float, float, float]): Coordinates of the IR point in space.
        """

        self._params = self.IR(a_file=pFile, x=pCoords[0], y=pCoords[1], z=pCoords[2])

        self._amplitude_at_position: np.array = None
        self._distance_to_point: float = 0.0
        self._weight: float = 0.0
        self._number_of_channels: int = 0
        self._er_as_array: np.ndarray = None
        self._tail_as_array: np.ndarray = None
        self._merged_array: np.ndarray = None
        self._warp_table: np.ndarray = None

        if pFile is not None:

            self.a_file = pFile

            # Reading the file (channel by channel, getting the samplerate)
            output, samplerate = sf.read(os.path.abspath(pFile))
            self._number_of_channels = output[0].size
            data = None

            # Initializing the array of amplitude with length _number_of_channels
            self._amplitude_at_position = np.array(self._number_of_channels)

            if self._number_of_channels == 1:
                data = output[:, np.newaxis]
            else:
                data = output

            # Getting the number of frames during the first 0.3 seconds (corresponds to the Early Reflection (ER) part of the IR).
            ER_LIMIT = 0.3
            num_frames_for_er = int(samplerate * ER_LIMIT)

            # Initializing the arrays' sizes
            self._er_as_array = np.ndarray(
                (self._number_of_channels, num_frames_for_er)
            )
            self._tail_as_array = np.ndarray(
                (self._number_of_channels, data[:, 0].size - num_frames_for_er - 1)
            )
            self._warp_table = np.ndarray(
                (self._number_of_channels, num_frames_for_er - 1)
            )

            # For each channel of the file, we store the early reflection part in the _as_array attribute
            # and the tail part in the _tail_as_array attribute (rows and columns are switched to match the interpolation logic)
            for channel in range(self._number_of_channels):
                self._er_as_array[channel] = data[:, channel][0:num_frames_for_er]
                self._tail_as_array[channel] = data[:, channel][num_frames_for_er:-1]

    def merge_arrays(self, num_of_channel: int) -> None:
        """A method to merge the Early Reflection array and the Tail array.

        Args:
            num_of_channel (int): the number of channels of the impulse response whose arrays want to be merge.
        """

        self._merged_array = np.ndarray(
            (num_of_channel, self._er_as_array[0].size + self._tail_as_array[0].size)
        )

        for channel in range(num_of_channel):
            
            # In order to generated an ambisonic wav file, we need to have a 2d-array. But the early_reflections of the 
            # wanted point are represented by a list of list. In order to get a 2d-array, one need to adjust the length 
            # of the lists, so that they match the size of the merged_array (which is a 2d-array). 

            diff = len(self._merged_array[channel]) - (len(self._er_as_array[channel])+len(self._tail_as_array[channel]))
            
            # If diff is 0, we simply add er_as_array and tail_as_array
            if diff == 0:
                self._merged_array[channel] = np.append(self._er_as_array[channel], self._tail_as_array[channel])
            # If diffe < 0, then we shorten the size of the tail_as_array
            elif diff < 0 :
                index_end_of_tail = len(self._tail_as_array[channel]) + diff 
                self._merged_array[channel] = np.append(
                    self._er_as_array[channel], self._tail_as_array[channel][0:index_end_of_tail]
                )
            # If diff > 0, then we add some zeros to the end of the tail_as_array.
            elif diff > 0 :
                fill_gap = np.zeros(diff)
                tmp = np.append(self._tail_as_array[channel], fill_gap)
                self._merged_array[channel] = np.append(self._er_as_array[channel], tmp)
                


    def get_distance_with(self, anIR) -> float:
        """A method that returns the distance in space between the current Point instance and the IR instance passed as argument.

        Args:
            anIR (IR): the impulse reponse for which we want to know the distance.

        Returns:
            float: the euclidian distance between the two points.
        """

        return math.sqrt(
            (anIR._params.x - self._params.x) ** 2
            + (anIR._params.y - self._params.y) ** 2
            + (anIR._params.z - self._params.z) ** 2
        )
